import unittest
from unittest import TestCase
import src.main as main_func

class Testing(TestCase):
    def test_when_result_is_pass(self):
        response = main_func.func(12,26)
        self.assertEqual(response,38)

if __name__ == '__main__':
    unittest.main()