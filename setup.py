from setuptools import find_namespace_packages, find_packages, setup

setup(name='Sivakarthik-Repo',
      version='1.0',
      packages=find_packages(where="src"),
      package_dir={'': 'src'},
      zip_safe=False)
